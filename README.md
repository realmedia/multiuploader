# MultiUploader for OpenAdstream

A desktop tool for upload multiple files at once on OpenAdStream build with [Node.js](https://nodejs.org/) and [Github's Electron](https://github.com/atom/electron/).

To use it, just enter with username and password with API permission, choose the account, enter with the campaign and creative's id, drag or choose all the files you want to upload and that's it!

Before use it, remember to configure the settings.json file located in the root of the app. In this file, you'll need to add the OpenAdstream's URL where the creative is located and the accounts intended to use in the app.

Here's an example:

```sh
{"url":"https://openadstream17.247realmedia.com", "accounts":["Account_1","Account_2"]}
```

## Instructions to build the App

There are many ways to release an Electron app. The one I prefer is to use electron-packager to generate a executable file.

More informations at: https://github.com/maxogden/electron-packager

### Build Example

In my case, I needed to build an Windows x64 version of the app. So I installed `electron-packager`:

```sh
npm install electron-packager -g
```

And build the app, which was located at my C:/ folder:

```sh
electron-packager c:\multiuploader\src MultiUploader --platform=win32 --arch=x64 --version=0.34.3 --asar --icon=c:\128.ico --version-string.CompanyName="AppNexus Latin America" --version-string.LegalCopyright="" --version-string.FileDescription="MultiUploader" --version-string.OriginalFilename="MultiUploader" --version-string.FileVersion="0.1.25.0" --version-string.ProductVersion="0.1.25" --version-string.ProductName="MultiUploader" --version-string.InternalName="MultiUploader"
```

This release can be downloaded [here](https://bitbucket.org/daniloliveira/multiuploader/downloads/MultiUploader-win32-x64.rar).

### Creating an Installer

If you prefer, you can use `electron-build` to generate an installer for the app. More informations at: https://github.com/loopline-systems/electron-builder

#### NOTE: You need to have installed [Node.js](https://nodejs.org/) and NPM (NodeJS comes with it installed) in order to build the app.