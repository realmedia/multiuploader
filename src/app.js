(function(){'use strict';

	var
	fs = require('fs'),
	soap = require('soap');

	var
	size = 0,
	files = [],
	accounts = document.querySelector('#account'),
	username = document.querySelector('#username'),
	password = document.querySelector('#password'),
	campaignId = document.querySelector('#campaignid'),
	creativeId = document.querySelector('#creativeid'),
	sendButton = document.querySelector('#sendButton'),
	creativeWeight = document.querySelector('#creativeWeight'),
	closeButton = document.querySelector('#closeButton'),
	clearButton = document.querySelector('#clearButton'),
	waitDialog = (function(){
		var dialog = document.body.appendChild(document.createElement('div'));
		dialog.style.display = 'none';
		dialog.innerHTML = document.querySelector('#wait-dialog').textContent;

		return {show: function(){dialog.style.display = 'block'}, hide: function(){dialog.style.display = 'none'}};
	}()),
	addFileSize = function(file) {
      files.push(file);
	  size += file.size;
      showFileSize();
    },
    removeFileSize = function(file) {
	  files.splice(files.indexOf(file), 1);
      size -= file.size;
      showFileSize();
    },
    showFileSize = function(){
      var prefixes = 'KMGTPEZY';
      if (size >= 1000) {
          var
          log1000 = parseInt(Math.floor(Math.log10(size)/3), 10),
          totalSize = Math.floor(size/Math.pow(1000, log1000))+prefixes[log1000-1];
      }
      creativeWeight.innerHTML = totalSize || 0;
    },
	settings = (function(){
		if(fs.existsSync('settings.json')){
			return JSON.parse(fs.readFileSync('settings.json', 'utf8'));
		} else {
			var settings = {url:"https://openadstream17.247realmedia.com", accounts: []};
			fs.writeFileSync('settings.json', JSON.stringify(settings), 'utf8');
			return settings;
		}
	}()),
	endpoint = `${settings.url}/oasapi/OaxApi?wsdl`;

	username.value = localStorage.username || "";
	password.value = localStorage.password || "";
	
	clearButton.addEventListener("click", function(){
		campaignId.value = '';
		creativeId.value = '';
		files.splice(0, files.length);
	});
	closeButton.addEventListener("click", function(){
		window.close();
	});
	
	settings.accounts.forEach(function(account){
		var option = document.createElement("option");
		option.text = account;
		option.value = account;
		accounts.add(option);
	});

	[campaignId, creativeId].forEach(function(item){
		item.addEventListener('paste', function(e){
		  e.preventDefault();
		  this.value = e.clipboardData.getData('text/plain').replace(/ /g,'');
		});
	});

	sendButton.addEventListener("click", function(){
		if(!(username.value && password.value && accounts.value && campaignId.value && creativeId.value && files.length)){
			alert("Please, fill all inputs and drop files to upload.");
			return;
		}

		localStorage.setItem('username', username.value);
		localStorage.setItem('password', password.value);
		
		waitDialog.show();
		try{
			var xml = 
			`<AdXML>
				<Request type="Creative">
					<Creative action="upload">
						<CampaignId>${campaignId.value}</CampaignId>
						<Id>${creativeId.value}</Id>`;
			
			files.forEach(function(file){
				xml += `<File contentType="${file.type}" encoding="base64" fileType="component" fileName="${file.name}">`;
				xml += fs.readFileSync(file.path, {encoding: 'base64'});
				xml += '</File>';
			});

			xml += '</Creative></Request></AdXML>';
			var args = {account: accounts.value, username: username.value, password: password.value, xml: xml};
			soap.createClient(endpoint, function(err, client) {
			  client.OasXmlRequest(args, function(err, response) {
				  waitDialog.hide();

				  var parseString = require("xml2js").parseString;
				  parseString(response.result, function (err, result) {
					var r = result.AdXML;
					if(r.Response[0].Exception){
						alert(r.Response[0].Exception[0]._);
					} else if(r.Response[0].Creative[0].Exception){
						alert(r.Response[0].Creative[0].Exception[0]._);
					} else {
						clearButton.click();
						alert("Files uploaded successfully");
					}
				  });
			  });
			});
		}catch(e){
			alert("Error");
		}
	});

	new Dropzone('#zone', {
		uploadMultiple: true,
  		autoProcessQueue: false,
  		createImageThumbnails: false,
		addRemoveLinks: "dictRemoveFile",
		init: function() {
			this.on("addedfile", addFileSize);
			this.on('removedfile', removeFileSize);

			clearButton.addEventListener("click", () => this.removeAllFiles());
		}
	});

})();